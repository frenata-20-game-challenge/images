FROM ubuntu:latest

RUN mkdir /app && useradd -m godot && chown godot:root /app

RUN apt update && apt install -y wget unzip libfontconfig-dev

RUN wget https://github.com/godotengine/godot/releases/download/4.1.3-stable/Godot_v4.1.3-stable_linux.x86_64.zip -O godot.zip && \
	unzip godot.zip && \
	mv Godot_* /usr/bin/godot && \
	rm godot.zip

WORKDIR /tmp
USER godot
RUN wget https://github.com/godotengine/godot/releases/download/4.1.3-stable/Godot_v4.1.3-stable_export_templates.tpz -O export_templates.tpz && \
	mkdir -p /home/godot/.local/share/godot/export_templates/4.1.3.stable && \
	unzip -p export_templates.tpz templates/web_release.zip >/home/godot/.local/share/godot/export_templates/4.1.3.stable/web_release.zip && \
	unzip -p export_templates.tpz templates/web_debug.zip >/home/godot/.local/share/godot/export_templates/4.1.3.stable/web_debug.zip && \
	rm export_templates.tpz

RUN ls

WORKDIR /app