FROM node:lts-buster-slim

RUN npm install -g netlify-cli --unsafe-perm=true

RUN useradd -m netlify

USER netlify